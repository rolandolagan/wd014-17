<div class="vcss-sidebar">
    <div class="">

    </div>
    <div>
        <h5 class="user-name my-2 p-2">Welcome: <span>{{isset(Auth::user()->fname) ? Auth::user()->fname : ""}}</span></h5>
    </div>
    <hr>
    <ul class="s-ul">
        <h5 class="s-main-nav"><span>Main Navigation</span></h5>
        <li class="s-li">
            <a class="s-links" href="/dashboard">
                <img class="s-icons" src="{{asset('images/icons/dashboard.svg')}}" alt="">
                <span>DashBoard</span>
            </a>
        </li>
        @auth
            @if (Auth::user()->role_id > 2)
            <li class="s-li">
                <a class="s-links" href="/manage-profile">
                    <img class="s-icons" src="{{asset('images/icons/users.svg')}}" alt="">
                    <span>Profile</span>
                </a>
            </li>
            @endif
        @endauth
        @auth
            @if(Auth::user()->role_id===1 || Auth::user()->role_id===2)
        <li class="s-li">
            <a class="s-links" href="/manage-users">
                <img class="s-icons" src="{{asset('images/icons/users.svg')}}" alt="">
                <span>Users</span>
            </a>
        </li>   
        <li class="s-li">
            <a class="s-links" href="/manage-categories">
                <img class="s-icons" src="{{asset('images/icons/2-squares.svg')}}" alt="">
                <span>Categories</span>
            </a>
        </li>
        <li class="s-li">
            <a class="s-links" href="/manage-brands">
                <img class="s-icons" src="{{asset('images/icons/2-squares.svg')}}" alt="">
                <span>Brands</span>
            </a>
        </li>
        <li class="s-li">
            <a class="s-links" href="/manage-suppliers">
                <img class="s-icons" src="{{asset('images/icons/supplier.svg')}}" alt="">
                <span>Suppliers</span>
            </a>
        </li>
            @endif
        @endauth
        <li class="s-li">
            <a class="s-links" href="/manage-customers">
                <img class="s-icons" src="{{asset('images/icons/customer.svg')}}" alt="">
                <span>Customers</span>
            </a>
        </li>
        <li class="s-li">
            <a class="s-links" href="/manage-products">
                <img class="s-icons" src="{{asset('images/icons/product.svg')}}" alt="">
                <span>Products</span>
            </a>
        </li>
        <li class="s-li">
            <a class="s-links" href="/manage-orders">
                <img class="s-icons" src="{{asset('images/icons/order.svg')}}" alt="">
                <span>Orders</span>
            </a>
        </li>
        {{-- <li class="s-li">
            <a class="s-links" href="/report">
                <img class="s-icons" src="{{asset('images/icons/report.svg')}}" alt="">
                <span>Report</span>
            </a>
        </li> --}}
        <li class="s-li">
            <form class="s-form" action="/logout" method="POST">
                @csrf
                <button class="s-button">
                    <img class="s-icons" src="{{asset('images/icons/logout.svg')}}" alt="">
                    <span>Logout</span>
                </button>
            </form>
        </li>
    </ul>
</div>