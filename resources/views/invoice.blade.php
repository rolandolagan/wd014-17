<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
    <script src="{{asset('js/bootstrap.min.js')}}" defer></script>
    <title>Invoice</title>
</head>
<body>
    <style>
        *{
            margin:0;
            padding:0;
            box-sizing: border-box;;
        }
        body{
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        .main-container{
            widows: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
        }
        .invoice-wrapper{
            width:100%;
            height: 100vh;
            background-color: #E5E5E5;
        }
        .invoice-title, .invoice-customer, .invoice-products{
            width: 100%;
        }
        .invoice-title{
            text-align: center;
            
        }
        .invoice-customer{
            padding: 0 20px;
            margin-top: 50px;
        }
        .invoice-products{
            padding: 0 20px;
            margin-top: 50px;
        }
    </style>
    <div class="main-container">
        <div class="invoice-wrapper">
            <h1 class="invoice-title">Invoice</h1>
            <div class="invoice-customer">
                <legend>Customer Information</legend>
                <div>
                    <label for="">Fullname: {{$order->customer->name}}</label>
                    <span></span>
                </div>
                <div>
                    <label for="">Address: {{$order->customer->address}}</label>
                    <span></span>
                </div>
                <div>
                    <label for="">Email: {{$order->customer->email}}</label>
                    <span></span>
                </div>
                <div>
                    <label for="">Contact: {{$order->customer->contact}}</label>
                    <span></span>
                </div>
            </div>
            <hr>
            <div class="invoice-products">
                <legend>Order information</legend>
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order->products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->brand->name}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->selling_price}}</td>
                            <td>{{$product->pivot->quantity}}</td>
                            <td>{{$product->selling_price * $product->pivot->quantity}}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><h5>Total:</h5></td>
                            <td></td>
                            <td>{{$order->total}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><h5>Status:</h5></td>
                            <td></td>
                            <td>{{$order->payment->name}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</body>
</html>