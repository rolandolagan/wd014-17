@extends('layouts.template')

@section('title', 'Brands')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Brands</h3>
            <hr>
            <form action="/add-brand" method="GET">
                @csrf
                <button class="btn vcss-btn btn-prime" type="submit">Add Brand</button>
            </form>
            <div>
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            {{-- <th>Status</th> --}}
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($brands as $brand)
                        <tr>
                            <td>{{$brand->id}}</td>
                            <td>{{$brand->name}}</td>
                            <td class="td-action">
                                <form action="/update-brand/{{$brand->id}}" method="GET">
                                    @csrf
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/edit.svg')}}" alt="">
                                    </button>
                                </form>
                                <form action="/delete-brand/{{$brand->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/delete.svg')}}" alt="">
                                    </button>
                                </form>
                            </td>
                        </tr>   
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection