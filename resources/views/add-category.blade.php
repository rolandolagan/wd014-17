@extends('layouts.template')

@section('title', 'Categories')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Add Category</h3>
            <hr>
            <div class="category-edit-wrapper">
                <legend>Category Information</legend>
                <hr>
                <form action="/add-category" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Category Name</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-prime vcss-btn btn-block" type="submit">Submit</button>
                        <a class="btn btn-back vcss-btn btn-block" href="/manage-categories" type="button">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection