@extends('layouts.template')

@section('title', 'Products')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Update Product</h3>
            <hr>
            <div class="product-edit-wrapper">
                <legend>Product Information</legend>
                <hr>
                <form class="form-scroll" action="/update-product/{{$product->id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="">Product Name</label>
                        <input class="form-control" type="text" name="name" value="{{$product->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Category</label>
                        <select class="form-control" name="category_id">
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}" {{$category->id === $product->category_id ? 'selected' : ""}}>{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Brand</label>
                        <select class="form-control" name="brand_id">
                            @foreach ($brands as $brand)
                            <option value="{{$brand->id}}" {{$brand->id === $product->brand_id ? 'selected' : ""}}>{{$brand->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <input class="form-control" type="text" name="description" value="{{$product->description}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Quantity</label>
                        <input class="form-control" type="number" name="quantity" value="{{$product->quantity}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Buying Price</label>
                        <input class="form-control" type="number" name="buying_price" value="{{$product->buying_price}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Selling price</label>
                        <input class="form-control" type="number" name="selling_price" value="{{$product->selling_price}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Supplier</label>
                        <select class="form-control" name="supplier_id">
                            @foreach ($suppliers as $supplier)
                            <option value="{{$supplier->id}}" {{$supplier->id === $product->supplier_id ? 'selected' : ""}}>{{$supplier->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-prime vcss-btn btn-block" type="submit">Submit</button>
                        <a class="btn btn-back vcss-btn btn-block" href="/manage-products" type="button">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection