@extends('layouts.template')

@section('title', 'Profile')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Profile</h3>
            <hr>
            <div class="profile-wrapper">
                <div>
                    <label for="">Firstname</label>
                    <input class="form-control" type="text" value="{{Auth::user()->fname}}" readonly>
                </div>
                <div>
                    <label for="">Lastname</label>
                    <input class="form-control" type="text" value="{{Auth::user()->lname}}" readonly>
                </div>
                <div>
                    <label for="">Email</label>
                    <input class="form-control" type="text" value="{{Auth::user()->email}}" readonly>
                </div>
                <div class="form-group my-3">
                    <form action="/update-profile/{{Auth::user()->id}}" method="GET">
                        @csrf
                        <button class="btn btn-primary btn-block" type-submit>Update</button>
                    </form>
                    <form class="my-2" action="/update-password/{{Auth::user()->id}}" method="GET">
                        @csrf
                        <button class="btn btn-warning btn-block" type-submit>Change Password</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection