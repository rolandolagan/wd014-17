@extends('layouts.template')

@section('title', 'Orders')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Order Information</h3>
            @if (Session::has("message"))
                <span class="text-danger">{{Session::get('message')}}</span>
            @endif
            <hr>
            <div class="ord-wrapper d-flex">
                <div class="ord-prod-div">
                    <legend>Product List</legend>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prod_list as $product)
                            <tr>
                                <td>{{$product->name}}</td>
                                <td class="td-qty">
                                    {{-- <form action="/update-qty/{{$product->id}}" method="POST">
                                        @csrf
                                        <div class="input-group">
                                            <input class="form-control" type="number" name="qty" value="{{$product->qty}}">
                                        <div class="input-group-append">
                                            <button class="btn btn-success updateQty" data-id="{{$product->id}}">update</button>
                                        </div>
                                        </div>
                                    </form> --}}
                                    <div class="input-group">
                                        <input class="form-control" type="number" name="qty" value="{{$product->qty}}">
                                        <div class="input-group-append">
                                            <button class="btn btn-success updateQty" data-id="{{$product->id}}">update</button>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$product->selling_price}}</td>
                                <td class="subs">{{$product->subtotal}}</td>
                                <td>
                                    <form action="/remove-prod/{{$product->id}}" method="GET">
                                        @csrf
                                        <button class="btn btn-danger" type="submit">Remove</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td><h5>Total:</h5></td>
                                <td></td>
                                <td></td>
                                <td id="total">{{$total}}</td>
                                <td><a class="btn btn-danger" href="/add-ordcart">Flash</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="ord-cus-div">
                    <div>
                        <legend>Customer Information</legend>
                        <div class="form-group">
                            <label for="">Fullname</label>
                            <select id="ord-select-cus" class="form-control">
                                <option value="">...</option>
                                @foreach ($customers as $customer)
                                <option value="{{$customer->id}}" data-address="{{$customer->address}}" data-email="{{$customer->email}}" data-contact="{{$customer->contact}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input class="form-control ord-cus-address" type="text" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control ord-cus-email" type="text" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Contact No.</label>
                            <input class="form-control ord-cus-cont" type="text" readonly>
                        </div>
                        @auth
                            @if (Auth::user()->role_id <= 2)
                            <div>
                                <label for="">Payment Status</label>
                                <select id="ord-select-pay" class="form-control">
                                    <option value="">...</option>
                                    @foreach ($payments as $payment)
                                    <option value="{{$payment->id}}">{{$payment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                        @endauth
                    </div>
                    <div class='form-group d-flex justify-content-center align-items-center'>
                        <form action="/add-order" method="POST">
                            @csrf
                            <input class="ord-cus" type="hidden" name="customer_id">
                            <input class="ord-pay" type="hidden" name="payment_id">
                            <input type="hidden" name="total" value="{{$total}}">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
<script type="text/javascript">
const selectCus = document.getElementById('ord-select-cus')
const selectPay = document.getElementById('ord-select-pay')
selectCus.addEventListener('change', function(){
    document.querySelector('.ord-cus-address').value = $('option:selected', this).attr('data-address')
    document.querySelector('.ord-cus-email').value = $('option:selected', this).attr('data-email')
    document.querySelector('.ord-cus-cont').value = $('option:selected', this).attr('data-contact')

    document.querySelector('.ord-cus').value = $('option:selected', this).attr('value')
})
selectPay.addEventListener('change', function(){
    document.querySelector('.ord-pay').value = $('option:selected', this).attr('value')
})
// $('#ord-select-cus').click(function(){
//     document.querySelector('.ord-cus-address').value = $('option:selected', this).attr('data-address')
//     document.querySelector('.ord-cus-email').value = $('option:selected', this).attr('data-email')
//     document.querySelector('.ord-cus-cont').value = $('option:selected', this).attr('data-contact')
// })
</script>
<script type="text/javascript" defer>
    const updateQty = document.querySelectorAll('.updateQty')

    updateQty.forEach(function(indivQty){
        indivQty.addEventListener('click',function(e){
            if(e.target.parentElement.previousElementSibling.value <= 0 ){
                alert("wrong input");
                e.target.parentElement.previousElementSibling.value = 1
            }
            const qty = e.target.parentElement.previousElementSibling.value
            const prod_id = e.target.getAttribute('data-id')
            let data = new FormData
            data.append("_token", "{{csrf_token()}}")
            data.append("qty", qty)
            fetch('/update-qty/' + prod_id, {
                method: "POST",
                body: data
            }).then(function(response){
                return response.text()
            }).then(function(data){
                const price = e.target.parentElement.parentElement.parentElement.nextElementSibling.textContent
                const subtotal = parseInt(price) * qty
                e.target.parentElement.parentElement.parentElement.nextElementSibling.nextElementSibling.textContent = subtotal
                
                const subs = document.querySelectorAll('.subs')
                let temp_total = 0
                for(var i = 0; i < subs.length; i++){
                    temp_total += parseInt(subs[i].textContent)
                }
                document.getElementById('total').textContent = temp_total
            })
        })
    })
</script>
{{-- <script src="{{asset('js/order.js')}}" defer></script> --}}
@endsection