@extends('layouts.template')

@section('title', 'Users')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Add User</h3>
            <hr>
            <div class="user-edit-wrapper">
                <legend>User Information</legend>
                <hr>
                <form class="form-scroll" action="/add-user" method="POST">
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="">First Name</label>
                            <input class="form-control" type="text" name="fname" required>
                        </div>
                        <div class="form-group">
                            <label for="">Last Name</label>
                            <input class="form-control" type="text" name="lname" required>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input id="email" class="form-control @error('email') is-invalid @enderror" type="email" name="email" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Confirm</label>
                            <input class="form-control" type="password" name="password_confirmation" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="role_id">
                                @foreach ($roles as $role)
                                @if ($role->id != 1)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="status_id">
                                @foreach ($statuses as $status)
                                <option value="{{$status->id}}">{{$status->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            <a class="btn btn-back vcss-btn btn-block" href="/manage-users" type="button">Back</a>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection