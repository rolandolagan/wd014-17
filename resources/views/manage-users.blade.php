@extends('layouts.template')

@section('title', 'Users')

@section('content')
<div class="vcss-main-div">
    <header class="vcss-header">
        <h2 class="h-header">Inventory Management System</h2>
    </header>
    
    <section class="vcss-section">
        <div class="vcss-div">
            <h3>Manage Users</h3>
            <hr>
            <form action="/add-user" method="GET">
                @csrf
                <button class="btn vcss-btn btn-prime" type="submit">Add User</button>
            </form>
            <div>
                <table class="text-center table table-striped my-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        @if ($user->id != 1)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->fname}}</td>
                            <td>{{$user->lname}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role->name}}</td>
                            <td>{{$user->status->name}}</td>
                            <td class="td-action">
                                <form action="/update-user/{{$user->id}}" method="GET">
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/edit.svg')}}" alt="">
                                    </button>
                                </form>
                                <form action="/delete-user" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <button class="btn vcss-btn">
                                        <img class="action-icons" src="{{asset('images/icons/delete.svg')}}" alt="">
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <footer class="vcss-footer">
        <p class="f-text">Created By: Me</p>
        <p class="f-text">Powered by Laravel 7v</p>
    </footer>
</div>
@endsection