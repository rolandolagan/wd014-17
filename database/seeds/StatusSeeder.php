<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->delete();
        DB::table('statuses')->insert(array(
            0 => array(
                'id'=>1,
                'name'=>'Active',
                'created_at'=>now(),
                'updated_at'=>now()
            ),
            1 => array(
                'id'=>2,
                'name'=>'Inactive',
                'created_at'=>now(),
                'updated_at'=>now()
            )
        ));
    }
}
