<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products(){
        return $this->belongsToMany('App\Product','prod_order')->withPivot('quantity')->withTimeStamps();
    }
    public function customer(){
        return $this->belongsTo('App\Customer');
    }
    public function payment(){
        return $this->belongsTo('App\Payment');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
