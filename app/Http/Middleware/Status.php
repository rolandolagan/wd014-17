<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Status
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() && Auth::user()->status_id === 1){
            return $next($request);
        }else{
            return back();
        }
    }
}
