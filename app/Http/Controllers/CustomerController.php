<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Product;
use App\Payment;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(){
        $customers = Customer::all();
        return view('manage-customers',compact('customers'));
    }
    public function create(){
        return view('add-customer');
    }
    public function store(Request $request){
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->email = $request->email;
        $customer->contact = $request->contact;
        $customer->save();
        return redirect('/manage-customers');
    }
     public function edit($id){
        $customer = Customer::find($id);
        return view('update-customer',compact('customer'));
     }
     public function update($id, Request $request){
        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->email = $request->email;
        $customer->contact = $request->contact;
     }
     public function destroy($id){
        $customer = Customer::find($id);
        $customer->delete();
        return back();
     }
}
