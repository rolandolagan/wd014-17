<?php

namespace App\Http\Controllers;

use User;
use App\Product;
use App\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $users = DB::table('users')->count();
        $paid = DB::table('orders')->where('payment_id',2)->count();
        $totalprod = DB::table('products')->count();
        $sale = DB::table('orders')->where('payment_id',2)->sum('total');
        return view('dashboard',compact('users','paid','totalprod','sale'));
    }
}
