<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Brand;
use App\Supplier;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request){
        $products = Product::all();
        $categories = Category::all();
        $brands = Brand::all();
        $suppliers = Supplier::all();
        $sort = $request->sort;
        $category = $request->category_id;
        $brand = $request->brand_id;
        if($category){
            $products = Product::where('category_id', $category)->get();
            if($brand){
                $products = Product::where('category_id', $category)->where('brand_id', $brand)->get();
            }
        }elseif($brand){
            $products = Product::where('brand_id', $brand)->get();
            if($category){
                $products = Product::where('brand_id', $brand)->where('category_id', $category)->get();
            }
        }
        if($category){
            $products = Product::where('category_id', $category)->get();
            if($brand){
                $products = Product::where('category_id', $category)->where('brand_id', $brand)->get();
            }
        }elseif($brand){
            $products = Product::where('brand_id', $brand)->get();
            if($category){
                $products = Product::where('brand_id', $brand)->where('category_id', $category)->get();
            }
        }

        if($sort === "asc"){
            $products = Product::orderBy('selling_price','asc')->get();
        }elseif($sort === "desc"){
            $products = Product::orderBy('selling_price','desc')->get();
        }
        
        // if($category){
        //     $products = Product::where('category_id', $category)->get();
        //     return $products->toJSON();
        // }
        return view('manage-products',compact('products','categories','brands','suppliers'));
    }
    public function create(){
        $categories = Category::all();
        $brands = Brand::all();
        $suppliers = Supplier::all();
        return view('add-product',compact('categories','brands','suppliers'));
    }
    public function store(Request $request){
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->buying_price = $request->buying_price;
        $product->selling_price = $request->selling_price;
        $product->supplier_id = $request->supplier_id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->save();
        return redirect('/manage-products');
    }
    public function pform(){
        $products = Product::all();
        $categories = Category::all();
        $brands = Brand::all();
        $suppliers = Supplier::all();
        return view('purchase-stock',compact('products','categories','brands','suppliers'));
    }
    public function addStock(Request $request){
        $product = Product::find($request->product_id);
        $product->quantity += $request->quantity;
        $product->buying_price = $request->buying_price;
        $product->selling_price = $request->selling_price;
        $product->save();
        return redirect('/manage-products');
    }
    public function edit($id){
        $product = Product::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        $suppliers = Supplier::all();
        return view('update-product',compact('product','categories','brands','suppliers'));
    }
    public function update($id, Request $request){
        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->buying_price = $request->buying_price;
        $product->selling_price = $request->selling_price;
        $product->supplier_id = $request->supplier_id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->save();
        return redirect('/manage-products');
    }
    public function destroy($id){
        $product = Product::find($id);
        $product->delete();
        return back();
    }
}
