<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Order;
use App\Customer;
use App\Payment;
use App\Product;
use App\User;
use PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request){
        $orders = Order::all();
        $customers = Customer::all();
        $payments = Payment::all();
        $products = Product::all();
        $users = User::all();
        if($request->price === "asc"){
            $orders = Order::orderBy('total','asc')->get();
        }elseif($request->price === "desc"){
            $orders = Order::orderBy('total','desc')->get();
        }else{
            $orders = Order::all();
        }
        return view('manage-orders',compact('orders','products','customers','payments','users'));
    }
    public function create(){
        $customers = Customer::all();
        $payments = Payment::all();
        $products = Product::all();
        return view('add-order',compact('customers','payments','products'));
    }

    public function store(Request $request){
        $order = new Order;
        if(Auth::user()->role_id > 2){
            $order->total = $request->total;
            $order->payment_id = 1;
            $order->customer_id = $request->customer_id;
            if(empty($order->customer_id)){
                Session::flash("message","Customer is not set");
                return back();
            }else{
                $order->user_id = Auth::user()->id;
                $order->save();
                $ordcart = Session::get('ordcart');
                foreach($ordcart as $id =>  $qty){
                    $order->products()->attach($id, ["quantity" => $qty]);
                    $product = Product::find($id);
                    $product->quantity -= $qty;
                    $product->save();
                }
                Session::forget('ordcart');
                return redirect('/manage-orders');
            }
        }else{
            $order->payment_id = $request->payment_id;
            $order->customer_id = $request->customer_id;
            if(empty($order->customer_id) && empty($order->payment_id)){
                Session::flash("message","Customer or Payment is not set");
                return back();
            }elseif(empty($order->customer_id)){
                Session::flash("message","Customer is not set");
                return back();
            }elseif(empty($order->payment_id)){
                Session::flash("message","Payment is not set");
                return back();
            }else{
                $order->total = $request->total;
                $order->user_id = Auth::user()->id;
                $order->save();
                $ordcart = Session::get('ordcart');
                foreach($ordcart as $id => $qty){
                    $order->products()->attach($id, ["quantity" => $qty]);
                    $product = Product::find($id);
                    $product->quantity -= $qty;
                    $product->save();
                }
                Session::forget('ordcart');
                return redirect('/manage-orders');
            }
        }
        
    }
    public function edit($id){
        $order = Order::find($id);
        $customers = Customer::all();
        $payments = Payment::all();
        return view('update-order',compact('order','payments'));
    }
    public function update($id, Request $request){
        $order = Order::find($id);
        // dd($request->payment_id);
        $order->payment_id = $request->payment_id;
        if ($order->payment_id === "2") {
            foreach($order->products as $prod){
                $prod->quantity -= $prod->pivot->quantity;
                $prod->save();
            }
        }
        $order->save();
        return redirect('/manage-orders');
    }
    public function destroy($id){
        $order = Order::find($id);
        foreach($order->products as $id){
            $order->products()->detach($id,["order_id" => $id]);
        }
        $order->delete();
        return back();
    }
    // download PDF
    public function details($id){
        $order = Order::find($id);
        $customers = Customer::all();
        $payments = Payment::all();
        return view('order-details',compact('order','payments'));
    }
    public function invoice($id){
        $order = Order::find($id);
        $customers = Customer::all();
        $payments = Payment::all();
        $filename = $order->customer->name.".pdf";
        $pdf = PDF::loadView('invoice',compact('order','payments'));
        return $pdf->download($filename);
        // return view('invoice',compact('order','payments'));
    }
}