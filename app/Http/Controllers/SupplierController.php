<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index(){
        $suppliers = Supplier::all();
        return view('manage-suppliers',compact('suppliers'));
    }
    public function create(){
        return view('add-supplier');
    }
    public function store(Request $request){
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->email = $request->email;
        $supplier->contact = $request->contact;
        $supplier->save();
        return redirect('/manage-suppliers');
    }
     public function edit($id){
        $supplier = Supplier::find($id);
        return view('update-supplier',compact('supplier'));
     }
     public function update($id, Request $request){
        $supplier = Supplier::find($id);
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->email = $request->email;
        $supplier->contact = $request->contact;
        $supplier->save();
        return redirect('/manage-suppliers');
     }

     public function destroy($id){
        $supplier = Supplier::find($id);
        $supplier->delete();
        return back();
     }

}
