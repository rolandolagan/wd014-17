<?php

namespace App\Http\Controllers;

use  App\Product;
use  App\Customer;
use  App\Payment;
use Session;
use Illuminate\Http\Request;

class OrdCartController extends Controller
{
    public function addproduct($id, Request $request){
        $product = Product::find($id);
        if($product->quantity <= 0){
            // this is just a message using session
            Session::flash("message","Product $product->name is out of stock");
            return back();
        }
        if(Session::has('ordcart')){
            $ordcart = Session::get('ordcart');
        }else{
            $ordcart = [];
        }

        if(!isset($ordcart[$id])){
            // $ordcart[$id] = $request->quantity;
            $ordcart[$id] = 1;
            Session::put('ordcart', $ordcart);
            Session::flash("message","Product $product->name is added");
            return back();
        }else{
            Session::flash("message","Product $product->name is already added");
            return back();
        }
    }

// this loads data to order-info.blade
    public function orderinfo(){
        $prod_list = [];
        $total = 0;
        $customers = Customer::all();
        $payments = Payment::all();

        if(empty(Session::get('ordcart'))){
            Session::flash("message","Please add a product");
            return back();
        }elseif (!empty(Session::get('ordcart'))) {
            if(Session::has('ordcart')){
                $ordcart = Session::get('ordcart');
                foreach($ordcart as $id => $prod_list_qty){
                    $product = Product::find($id);
                    // attach the additional columns qty and subtotal
                    $product->qty = $prod_list_qty;
                    $product->subtotal = $prod_list_qty * $product->selling_price;
                    $prod_list[] = $product;
                    $total += $product->subtotal;
                }
            }
            return view('order-info',compact('prod_list','total','customers','payments'));
        }else{
            Session()->forget('ordcart');
            return redirect('/manage-orders');
        }
        
    }

    public function updateQty($id, Request $request){

        $ordcart = Session::get('ordcart');
        $product = Product::find($id);
        if($request->qty > $product->quantity){
            Session::flash("message","Invalid input, current stock of $product->name is: $product->quantity");
            return back();
        }elseif($request->qty <= 0){
            Session::flash("message","Product quantity limit is 1");
            $ordcart[$id] = 1;
            return back();
        }else{
            Session::flash("message","Product $product->name quantity is updated");
            $ordcart[$id] = (int)$request->qty;
            Session::put('ordcart', $ordcart);
            return back();
        }
    }

    public function remove($id){
        if(!empty(Session::get('ordcart'))){
            Session()->forget('ordcart');
            return redirect('/manage-orders');
        }else{
            Session::flash("message","Product has been removed!");
            Session::forget("ordcart.$id");
            return back();
        }
    }
    public function flash(){
        // Session::forget('ordcart);
        Session()->forget('ordcart');
        return redirect('/manage-orders');
    }
}
