<?php

namespace App\Http\Controllers\Auth;


use App\User;
use App\Role;
use App\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        $roles = Role::all();
        $statuses = Status::all();
        return view('auth/registration', compact('roles','statuses'));
    }
    public function register(Request $request){
        $this->validation($request);
        // or $request['password'] = bcrypt($request->password);
        // User::create($request->all());
        User::create([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => $request->role_id,
            'status_id' => $request->status_id,
        ]);
        return back();
    }
    public function validation($request){

        return $this->validate($request,[
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|string|min:8|confirmed|max:255',
        ]);
    }
}
