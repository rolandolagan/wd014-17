<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(){
        return view('auth/login');
    }

    public function login(Request $request){
        $this->validate($request, User::$login_validation);

        // $this->validate($request,[
        //     'email' => 'required|email|max:255',
        //     'password' => 'required|password:api|max:255',
        // ]);

        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            return redirect('/dashboard');
        }else{ 
            return back()->withInput()->withErrors(['email'=>'Username or password is invalid']);
        }
    }

    public function logout(Request $request){
        if(Auth::logout()){
            Auth::logout();
            $request->session()->invalidate();
        }
        Session()->forget('ordcart');
        return redirect('/');
    }
}
